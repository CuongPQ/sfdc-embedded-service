//
//  AppDelegate.h
//  HelloServiceCloud
//
//  Created by CuongPQ on 8/14/18.
//  Copyright © 2018 com.GMS. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

