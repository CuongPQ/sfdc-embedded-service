//
//  ViewController.m
//  HelloServiceCloud
//
//  Created by CuongPQ on 8/14/18.
//  Copyright © 2018 com.GMS. All rights reserved.
//

#import "ViewController.h"
@import ServiceCore;
@import ServiceSOS;
@import ServiceChat;
@import ServiceCases;
@import ServiceKnowledge;

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)launchSOS:(id)sender {
    SOSOptions *options = [SOSOptions optionsWithLiveAgentPod:@"d.la1-c1-ukb.salesforceliveagent.com"
                                                        orgId:@"00D2v000001dwoh"
                                                 deploymentId:@"0NW7F000000XZXa"];
    
    [[SCServiceCloud sharedInstance].sos startSessionWithOptions:options];
}

- (IBAction)launchChat:(id)sender {
    
    
    SCSChatConfiguration *config =
    [[SCSChatConfiguration alloc] initWithLiveAgentPod:@"[//@TODO: Parse Chat Endpoint URL]"
                                                 orgId:@"[//@TODO: Salesforce ORG ID]"
                                          deploymentId:@"[//@TODO: Parse Id of Chat's Deployment]"
                                              buttonId:@"[//@TODO: Parse Id of Chat's Button]"];
    // Create a hidden field
    SCSPrechatObject* customDataCase = [[SCSPrechatObject alloc]
                                    initWithLabel:@"Subject"
                                    value:@"[VINID] need help"];
    SCSPrechatObject* customDataCaseRecordTypeId = [[SCSPrechatObject alloc]
                                        initWithLabel:@"RecordTypeId"
                                        value:@"[//@TODO: Parse Record Type Id of Case if have]"];
    SCSPrechatObject* customDataCaseOrigin = [[SCSPrechatObject alloc]
                                                    initWithLabel:@"Origin"
                                                    value:@"Chat"];
    SCSPrechatObject* customDataCaseStatus = [[SCSPrechatObject alloc]
                                              initWithLabel:@"Status"
                                              value:@"New"];
    
    // Add field to SCSChatConfiguration object
    [config.prechatFields addObject:customDataCase];
    [config.prechatFields addObject:customDataCaseRecordTypeId];
    [config.prechatFields addObject:customDataCaseOrigin];
    [config.prechatFields addObject:customDataCaseStatus];
    
    // Create a hidden field
    SCSPrechatObject* customDataContactName = [[SCSPrechatObject alloc]
                                    initWithLabel:@"LastName"
                                    value:@"Pham Duy Minh"];
    
    // Add field to SCSChatConfiguration object
    [config.prechatFields addObject:customDataContactName];
    
    // Create a hidden field
    SCSPrechatObject* customDataContactEmail = [[SCSPrechatObject alloc]
                                               initWithLabel:@"Email"
                                               value:@"minhphambk08@gmail.com"];
    
    // Add field to SCSChatConfiguration object
    [config.prechatFields addObject:customDataContactEmail];
    
    // Create a field
    SCSPrechatEntityField* entityFieldEmail = [[SCSPrechatEntityField alloc]
                                          initWithFieldName:@"Email" label:@"Email"];
    // Create a field
    SCSPrechatEntityField* entityFieldName = [[SCSPrechatEntityField alloc]
                                               initWithFieldName:@"LastName" label:@"LastName"];
    
    // Create an entity
    SCSPrechatEntity* entity = [[SCSPrechatEntity alloc]
                                initWithEntityName:@"Contact"];
    
    // Add fields to entity
    [entity.entityFieldsMaps addObject:entityFieldName];
    // Add fields to entity
    [entity.entityFieldsMaps addObject:entityFieldEmail];
    
    // Create a field
    SCSPrechatEntityField* entityFieldSubject = [[SCSPrechatEntityField alloc]
                                               initWithFieldName:@"Subject" label:@"Subject"];
    SCSPrechatEntityField* entityFieldRecordTypeId = [[SCSPrechatEntityField alloc]
                                                 initWithFieldName:@"RecordTypeId" label:@"RecordTypeId"];
    SCSPrechatEntityField* entityFieldOrigin = [[SCSPrechatEntityField alloc]
                                                      initWithFieldName:@"Origin" label:@"Origin"];
    SCSPrechatEntityField* entityFieldStatus = [[SCSPrechatEntityField alloc]
                                                      initWithFieldName:@"Status" label:@"Status"];

    // Create an entity
    SCSPrechatEntity* entityCase = [[SCSPrechatEntity alloc]
                                initWithEntityName:@"Case"];
    // Add fields to entity
    [entityCase.entityFieldsMaps addObject:entityFieldSubject];
    entityFieldSubject.doCreate = YES;
    [entityCase.entityFieldsMaps addObject:entityFieldRecordTypeId];
    [entityCase.entityFieldsMaps addObject:entityFieldOrigin];
    [entityCase.entityFieldsMaps addObject:entityFieldStatus];
    entityCase.saveToTranscript = @"Case";    // Save this entity to Transcript.ContactId
    entityCase.showOnCreate = true;

    
    // Create an entity
    entity.saveToTranscript = @"ContactId";    // Save this entity to Transcript.ContactId
    entity.linkToEntityName = @"Case";
    entity.linkToEntityField = @"ContactId"; // Link this entity to Case.ContactId
    entity.showOnCreate = true;
    
    // Add an entity field map to our entity
    SCSPrechatEntityField* entityField = [[SCSPrechatEntityField alloc]
                                          initWithFieldName:@"LastName" label:@"LastName"];
    entityField.doFind = YES;        // Attempt to search for that field
    entityField.isExactMatch = YES;  // Must be an exact match
    entityField.doCreate = YES;      // Create if not found
    [entity.entityFieldsMaps
     addObject:entityField];        // Add field to entity map
    
    [config.prechatEntities addObject:entity];
    [config.prechatEntities addObject:entityCase];

    // Start the session
    [[SCServiceCloud sharedInstance].chat startSessionWithConfiguration:config];
}

- (IBAction)launchKnowledgeBase:(id)sender {
    [[SCServiceCloud sharedInstance].knowledge setInterfaceVisible:YES
                                                          animated:YES
                                                        completion:nil];
}

- (IBAction)launchCasePublisher:(id)sender {
    
    [[SCServiceCloud sharedInstance].cases setInterfaceVisible:YES
                                                      animated:YES
                                                    completion:nil];
}

@end
